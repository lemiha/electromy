import numpy as np

with open("top_10_predicted_indices.txt") as f:
    indices = f.readlines()

with open("lef_mcsa/filtered_test.txt") as f:
    bond_changes = f.readlines()

indices = [x.strip() for x in indices]
indices = [int(x) for x in indices]
indices = np.array(indices)


bond_changes = [x.strip() for x in bond_changes]
bond_changes = [x.split(" ")[1].split(";") for x in bond_changes]
bond_changes = [len(x) for x in bond_changes]
bond_changes = np.array(bond_changes)
bond_changes = bond_changes[indices]

print(np.mean(bond_changes))
print(min(bond_changes))
print(max(bond_changes))