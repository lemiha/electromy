import numpy as np

f_in = "results.txt"

with open(f_in) as f:
    topk = f.readlines()

topk = [x.strip() for x in topk]
topk = [float(x.split("is ")[1])*100 for x in topk]
topk = np.round(np.array(topk),1)
topk = [str(x) for x in topk]

print("\\newcommand{\\toponeelectro}{"+ topk[0] +"}")
print("\\newcommand{\\toptwoelectro}{"+ topk[1] +"}")
print("\\newcommand{\\toptenelectro}{"+ topk[9] +"}")
print("")
print("\\newcommand{\\toponerexgen}{7.3}")
print("\\newcommand{\\toptworexgen}{9.7}")
print("\\newcommand{\\toptenrexgen}{20.5}")

print("")
print("")
print("    \\begin{tabular}{|c|r|r|r|r|r|r|r|r|r|r|}")
print("        \\hline")
print("        \\textbf{Top-}$\\boldsymbol{k}$ & \\multicolumn{1}{c|}{\\textbf{1}} & \\multicolumn{1}{c|}{\\textbf{2}} & \\multicolumn{1}{c|}{\\textbf{3}} & \\multicolumn{1}{c|}{\\textbf{4}} & \\multicolumn{1}{c|}{\\textbf{5}} & \\multicolumn{1}{c|}{\\textbf{6}} & \\multicolumn{1}{c|}{\\textbf{7}} & \\multicolumn{1}{c|}{\\textbf{8}} & \\multicolumn{1}{c|}{\\textbf{9}} & \\multicolumn{1}{c|}{\\textbf{10}} \\\\ \\hline")
print("        \\electro{} & \\toponeelectro{}\\% & \\toptwoelectro{}\\% & " + "\\% & ".join(topk[2:9]) + " \\% & \\toptenelectro{}\\% \\\\ \\hline")
print("        \\rexgen{} & \\toponerexgen{}\\% & \\toptworexgen{}\\% & 12.1\\% & 13.6\\% & 15.1\\% & 16.9\\% & 18.7\\% & 18.7\\% & 19.3\\% & \\toptenrexgen{}\\%  \\\\ \\hline")
print("    \\end{tabular}")

