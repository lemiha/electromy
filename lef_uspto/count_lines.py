import numpy as np

f_test = open("filtered_test.txt", "r")
f_train = open("filtered_train.txt", "r")
f_valid = open("filtered_valid.txt", "r")

content = f_test.readlines() + f_train.readlines() + f_valid.readlines()
content = [x.strip() for x in content]
content = [x.split(" ")[1].split(";") for x in content]
content = [len(x) for x in content]

print(np.mean(content))
print(min(content))
print(max(content))