import typing
import itertools

import numpy as np
import torch
from rdkit import Chem

from graph_neural_networks.sparse_pattern import graph_as_adj_list
from graph_neural_networks.core import nd_ten_ops
from graph_neural_networks.core import utils

from .rdkit_ops import rdkit_reaction_ops
from .rdkit_ops import rdkit_general_ops
from .rdkit_ops import rdkit_featurization_ops
from .rdkit_ops import chem_details
from . import lef_consistency_errors
from . import action_list_funcs
from . import mask_creators
from . import graph_ds
from ..misc import data_types


"""
 ==== Datastructures from the different transforms ====
"""


class BrokenDownParts(typing.NamedTuple):
    reactants: str  # AM-SMILES
    reagents: str  # AM-SMILES
    products: str  # AM-SMILES
    ordered_am_path: typing.List[int]  # should include self-bond break at start if applicable by repeated action.


class RdKitIntermediates(typing.NamedTuple):
    intermediates: typing.List[Chem.Mol]
    reagents: Chem.Mol
    ordered_indx_path: typing.List[int]
    am_to_idx_map: typing.Mapping[int, int]


class ElectroTrainInput:
    def __init__(self,
            graphs: graph_as_adj_list.GraphAsAdjList,
            reagents: graph_as_adj_list.GraphAsAdjList,

            initial_input: graph_ds.ActionSelectorGraphIds,
            remove_input: graph_ds.ActionSelectorGraphIds,
            add_input: graph_ds.ActionSelectorGraphIds,

            initial_target: graph_ds.ActionSelectorTarget,
            remove_target: graph_ds.ActionSelectorTarget,
            add_target: graph_ds.ActionSelectorTarget,

            stop_label: nd_ten_ops.Nd_Ten,
            stop_mask: nd_ten_ops.Nd_Ten,

            num_reactions:int):
        self.graphs = graphs
        self.reagents = reagents

        self.initial_input = initial_input
        self.remove_input = remove_input
        self.add_input = add_input

        self.initial_target = initial_target
        self.remove_target = remove_target
        self.add_target = add_target

        self.stop_label = stop_label
        self.stop_mask = stop_mask
        self.num_reactions = num_reactions

    def to_torch(self, cuda_details: utils.CudaDetails):
        self.graphs = self.graphs.to_torch(cuda_details)
        self.reagents = self.reagents.to_torch(cuda_details)
        def func(op_arr: typing.Optional[np.ndarray]):
            if op_arr is not None:
                if op_arr.dtype == np.bool_:
                    op_arr = op_arr.astype(np.uint8)  # torch has no bool_ type.
                return torch.from_numpy(op_arr).to(cuda_details.device_str)
            else:
                return None

        self._over_nd_ten_fields(func)
        return self

    def _over_nd_ten_fields(self, func):
        for inp_ in [self.initial_input, self.remove_input, self.add_input]:
            inp_.graphs_ids = func(inp_.graphs_ids)
            inp_.reagent_context_ids = func(inp_.reagent_context_ids)
            inp_.prev_action_per_graph = func(inp_.prev_action_per_graph)

        for inp_ in [self.initial_target, self.remove_target, self.add_target]:
            inp_.action_mask = func(inp_.action_mask)
            inp_.label_per_graph = func(inp_.label_per_graph)

        self.stop_label = func(self.stop_label)
        self.stop_mask = func(self.stop_mask)




def check1(reactants, products, bond_changes):
    # A.2 We first create the path (checks for C1.)
    
    change_list = bond_changes.split(';')
    atom_pairs = np.array([c.split('-') for c in change_list]).astype(int)
    unordered_electron_path_am = action_list_funcs.actions_am_from_pairs(atom_pairs, consistency_check=True)

    # B Using these actions we can now work out what is a reagents and split these out.
    reactants, reagents, products = rdkit_reaction_ops.split_reagents_out_from_reactants_and_products(
        reactants, products, unordered_electron_path_am)

    # A.3 We can now order the electron path.
    reactant_mol = rdkit_general_ops.get_molecule(reactants, kekulize=False)
    product_mol = rdkit_general_ops.get_molecule(products, kekulize=False)
    ordered_electon_path_am = action_list_funcs.order_actions_am(unordered_electron_path_am, reactant_mol, product_mol)

    # A.3b Work out whether to add add a self-bond remove at the start
    # (we need to start with a remove action to pick up a pair of electrons)
    first_bond_in_reactants = rdkit_general_ops.get_bond_double_between_atom_mapped_atoms(reactant_mol, ordered_electon_path_am[0],
                                                                                    ordered_electon_path_am[1])
    first_bond_in_products = rdkit_general_ops.get_bond_double_between_atom_mapped_atoms(product_mol, ordered_electon_path_am[0],
                                                                                ordered_electon_path_am[1])
    starts_already_with_remove_bond = first_bond_in_reactants - first_bond_in_products > 0
    if not starts_already_with_remove_bond:
        ordered_electon_path_am = [ordered_electon_path_am[0]] + ordered_electon_path_am

    # We can now create the o/p data-structure
    op = BrokenDownParts(reactants, reagents, products, ordered_electon_path_am)
    return op

def check2(reaction_parts: BrokenDownParts):
    # 1. Work out the number of add/remove steps.
    # We always start on a remove bond (can be remove self bond though) and go in intermediate add/remove steps)
    # Therefore the add remove steps is as follows:
    action_types = [bond_change for bond_change, _ in
                zip(
                    itertools.cycle([chem_details.ElectronMode.REMOVE, chem_details.ElectronMode.ADD]),
                    range(len(reaction_parts.ordered_am_path) -1)
                )]

    # 2. Form the intermediate states
    reactant_mol = rdkit_general_ops.get_molecule(reaction_parts.reactants, kekulize=True)
    reactant_atom_map_to_idx_map = rdkit_general_ops.create_atom_map_indcs_map(reactant_mol)
    intermediates = [reactant_mol, reactant_mol]  # twice as after picked up half a pair nothing has happened.
    action_pairs = zip(reaction_parts.ordered_am_path[:-1], reaction_parts.ordered_am_path[1:])
    for step_mode, (start_atom_am, next_atom_am) in zip(action_types, action_pairs):
        # Nb note we do not change any Hydrogen numbers on the first and last step -- these may change in practice
        # eg gaining H from water in solution, however, we do not represent Hydrogens in our graph structure,
        # apart from in the features.
        prev_mol = intermediates[-1]
        start_atom_idx = reactant_atom_map_to_idx_map[start_atom_am]
        next_atom_idx = reactant_atom_map_to_idx_map[next_atom_am]
        if start_atom_idx == next_atom_idx:
            # then a self bond removal:
            intermed = rdkit_reaction_ops.change_mol_atom(prev_mol, step_mode, start_atom_idx)
        else:
            intermed = rdkit_reaction_ops.change_mol_bond(prev_mol, step_mode, (start_atom_idx, next_atom_idx))
        intermediates.append(intermed)

    # 3. Form the reagent molecule
    reagent_mol = rdkit_general_ops.get_molecule(reaction_parts.reagents, kekulize=True)

    # 4. Consistency checks.
    product_mol = rdkit_general_ops.get_molecule(reaction_parts.products)
    # we first check C2:
    if not rdkit_reaction_ops.is_it_alternating_add_and_remove_steps(reactant_mol, product_mol,
                                                                        reaction_parts.ordered_am_path):
        raise lef_consistency_errors.NotAddingAndRemovingError

    if not rdkit_reaction_ops.is_sub_mol_consistent_with_super(product_mol, intermediates[-1]):
        raise lef_consistency_errors.InconsistentActionError(f"Inconsistent action error for molecule:"
                                                                f" {[str(part) for part in reaction_parts]}")

    # 5. Switch ordered path to refer to atoms indices rather than atom mapped number.
    ordered_index_path = [reactant_atom_map_to_idx_map[am] for am in reaction_parts.ordered_am_path]

    # 6. Create the final output
    op = RdKitIntermediates(intermediates, reagent_mol, ordered_index_path, reactant_atom_map_to_idx_map)

def run():
    with open("../../lef_mcsa/filtered_valid.txt") as f:
        lines = f.readlines()

    lines = [line.strip() for line in lines]

    count = 0
    f_tmp = open("tmp.txt", "w+")
    for line in lines:
        tmp = line.split(">>")
        reac = tmp[0]
        prod = tmp[1].split(" ")[0]
        b_c = tmp[1].split(" ")[1]


        try:
            broken_up_part = check1(reac, prod, b_c)
            check2(broken_up_part)
            f_tmp.write(reac + ">>" + prod + " " + b_c + "\n")
        except:
            print(reac)
            print(prod)
            print(b_c)
            count += 1
    
    print(count)
